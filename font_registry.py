import pygame

class FontRegistry():

    fonts = {}

    @staticmethod
    def get_font(size: int):
        if size in FontRegistry.fonts.keys():
            return FontRegistry.fonts[size]
        else:
            FontRegistry.fonts[size] = pygame.font.SysFont("comicsansms", size)
            return FontRegistry.fonts[size]