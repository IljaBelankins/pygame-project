import pygame
from bullets import Bullet
from user_events import ADDPLAYERBULLET, STOPPLAYERBULLET
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_z,
    K_x,
    K_c,
    KEYDOWN,
    QUIT,
)

"""
The player class is user controlled. It renders the player object using an image and adjusts the hitbox based on image resolution.
It also contains the necessary code to make movement and attacking possible.
"""

class Player (pygame.sprite.Sprite):
    
    def __init__ (self, image_in:str, screen_width_in:int, screen_height_in:int):
        super(Player, self).__init__()
        self.surf = pygame.image.load(image_in).convert()
        self.surf.set_colorkey ((255, 255, 255))
        self.rect = self.surf.get_rect()
        self.screen_width = screen_width_in
        self.screen_height = screen_height_in
        self.damage = 1
        self.health = 5
        self.waves = 1
        self.number_of_bullets = 1
        self.bullet_sprite = "playerbullet.jpg"

    def update(self, pressed_keys, can_player_fire, player_entities, all_sprites, player_shield_group):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -10)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 10)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-10, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(10, 0)
        if can_player_fire == True and pressed_keys[K_x]:
            player_bullet = Bullet ("playerbullet.jpg", self.rect, 5, 0, 0, 0)
            player_entities.add(player_bullet)
            all_sprites.add(player_bullet)
        if can_player_fire == True and pressed_keys[K_c]:
            player_shield = Bullet ("shieldedit.jpg", self.rect, 5, 0, 0, 0)
            player_shield_group.add(player_shield)
            all_sprites.add(player_shield)

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > self.screen_width - 250:
            self.rect.right = self.screen_width - 250
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= self.screen_height:
            self.rect.bottom = self.screen_height

        
    def launch_player_bullet (self):
        pygame.time.set_timer(ADDPLAYERBULLET, 200, False)
        pygame.time.set_timer(STOPPLAYERBULLET, 200*self.waves , True)