import pygame
from pygame.locals import (
    RLEACCEL
)
from font_registry import FontRegistry
from text import UIText

"""
A class that produces an interface that the player can interact with to take actions in combat.
"""

class CombatInterface (pygame.sprite.Sprite):

    def __init__(self, combat_interface_sprite_in:str, screen_width_in:int, screen_height_in:int):
        super(CombatInterface, self).__init__()
        self.surf = pygame.image.load(combat_interface_sprite_in).convert()
        self.surf.set_colorkey ((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
            screen_width_in/2, 
            screen_height_in - self.surf.get_height()/2
            )
        )

        self.text = UIText(self.surf.get_width()/2, self.surf.get_height()/2, "Fight")

    def render(self, screen):
        self.text.render(self.surf)
        screen.blit(self.surf, self.rect)

        
