import json
from typing import Dict
from random import randrange
import pygame
import math
from attacks import Attack
from bullets import Bullet
from player import Player
from enemy import Enemy
from font_registry import FontRegistry
from combat_ui import CombatInterface
from combat_background import Combat_Background
from user_events import ADDENEMY, ADDENEMYBULLET, ADDPLAYERBULLET, STOPENEMYBULLET, STOPPLAYERBULLET, ADDPLAYERSHIELD, STOPPLAYERSHIELD
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_z,
    K_x,
    K_p,
    K_o,
    KEYDOWN,
    QUIT,
)

"""
Screen resolution setting for the game.
"""
screen_width = 800
screen_height = 600

"""
Loading and reading json file containing all enemy stats and attack patterns.
"""

file = open ("enemies_attack_patterns.json", "r")
reading = file.read()
enemies_attacks_databank = json.loads(reading)

"""
Initialising pygame module. Adding necessary code for module to run. Creating sprite groups for rendering and collision purposes.
"""

pygame.init()

screen = pygame.display.set_mode((screen_width, screen_height))

player = Player("PLAYER.png", screen_width, screen_height)

enemies = pygame.sprite.Group()
enemy_bullets = pygame.sprite.Group()
background = pygame.sprite.GroupSingle()
all_sprites = pygame.sprite.Group()
player_entities = pygame.sprite.Group()

all_sprites.add(player)

player_shield_group = pygame.sprite.Group()
player_shield = Bullet ("shieldedit.jpg", player.rect, 5, 0, 0, 0)
player_shield_group.add(player_shield)
regular_shield = True

"""
This allows combat to initialise and end correctly when the player wins. On player loss, the player dies and the game quits.
"""

spawn_enemy_init = False
killed_by_player = False
running = True

"""
Creating enemy using enemy class and adding it to necessary sprite groups.
"""

enemy = Enemy("troll_enemy.png", "background.png", enemies_attacks_databank["orc"], "bullet.jpg", screen_width, screen_height)
new_enemy = enemy
enemies.add(new_enemy)
all_sprites.add(new_enemy)

"""
Creating the enemy's attack by using a function in the class that reads the json file.
"""

enemy_attack = enemy.get_attack()

"""
Creating a background for the combat by using the enemy object.
"""

combat_background = Combat_Background(enemy.enemy_background)
background.add(combat_background)

"""
Timers for player's attack and shield.
"""

"""
///THIS IS WHERE THE COMBAT INTERFACE WILL BE INSTATIATED///
"""

combat_interface = CombatInterface("combat_interface.png", screen_width, screen_height)


pygame.time.set_timer(ADDPLAYERBULLET, 400, False)
pygame.time.set_timer(ADDPLAYERSHIELD, 1000, False)

"""
Loop for the game.
"""

while running:
    clock = pygame.time.Clock()
    can_player_fire = False

    for event in pygame.event.get():
        if event.type == ADDENEMYBULLET:
            print ("test")
            print ("test1")
            enemy_attack.launch_bullets_enemy(enemy, enemy_bullets, all_sprites)
        if event.type == STOPENEMYBULLET:
            pygame.time.set_timer(ADDENEMYBULLET, 0, True)

        if event.type == ADDPLAYERBULLET:
            can_player_fire = True
        if event.type == ADDPLAYERSHIELD:
            can_player_shield = True

        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
            if event.key == K_z:
                enemy_attack.trigger_attack()
            if event.key == K_p:
                regular_shield = False
            if event.key == K_o:
                player.damage = 5
        if event.type == QUIT:
            running = False

    if Enemy.alive == False and killed_by_player == False:
        spawn_enemy_init = True
    
    if spawn_enemy_init == True:
        ADDENEMY
        enemy.alive = True
           
    pressed_keys = pygame.key.get_pressed()

    player.update(pressed_keys, can_player_fire, player_entities, all_sprites, player_shield_group)
    enemies.update()
    enemy_bullets.update()
    player_entities.update()
    player_shield_group.update()

    screen.fill ((0, 0, 0))
    for entity in background:
        screen.blit (entity.surf, entity.rect)
    for entity in all_sprites:
        screen.blit (entity.surf, entity.rect)
        
    combat_interface.render(screen)

    

    if pygame.sprite.spritecollideany(player, enemy_bullets):
        player.health = player.health - 1
        print ("damage taken")
        checking_bullets = enemy_bullets.sprites()
        for entity in checking_bullets:
            entity.remove(enemy_bullets)
            entity.remove(all_sprites)
        enemy_bullets.empty()

    if pygame.sprite.spritecollideany(new_enemy, player_entities):
        enemy.health = enemy.health - player.damage
        print (enemy.health)
        checking_player_bullets = player_entities.sprites()
        checking_all_sprites = all_sprites.sprites()
        for entity in checking_player_bullets: 
            entity.remove(player_entities)
            entity.remove(all_sprites)

    if pygame.sprite.groupcollide(player_shield_group, enemy_bullets, regular_shield, True):
        player_shield.remove
        for entity in enemy_bullets:
            entity.remove


            
            
    if enemy.health == 0:
        new_enemy.remove(all_sprites)
        new_enemy.remove(enemies)

            

        


    if player.health == 0:
        player.kill
        running = False

    pygame.display.flip()
    clock.tick (30)

