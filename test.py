class Enemy (pygame.sprite.Sprite):
    def __init__ (self):
        super(Enemy, self).__init__()
        self.surf = pygame.image.load("troll_enemy.png").convert()
        self.surf.set_colorkey ((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                screen_width/2,
                screen_height/2
            )
        )
        self.alive = False
    def update (self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill