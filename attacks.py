import pygame
from player import Player
from bullets import Bullet
from user_events import ADDENEMYBULLET, ADDPLAYERBULLET, STOPENEMYBULLET, STOPPLAYERBULLET

"""
This class works symbiotically with the enemy class and the bullets class to create attack patterns.
"""

class Attack():
    
    def __init__(self, sprite_in:str, number_of_bullets_in:int = 3, degrees_in:int = 45, waves_in:int = 5):
        print ("Working")
        self.number_of_bullets = number_of_bullets_in
        self.degrees = degrees_in
        self.waves = waves_in
        self.sprite = sprite_in

    def trigger_attack (self):
        pygame.time.set_timer(ADDENEMYBULLET, 200, False)
        pygame.time.set_timer(STOPENEMYBULLET, 200*self.waves , True)

    def launch_bullets_enemy (self, enemy_in,enemy_bullets_in, all_sprites_in):
        for index in range(self.number_of_bullets):
            new_bullet = Bullet (self.sprite, enemy_in.rect, -5, 0 + index*self.degrees)
            enemy_bullets_in.add(new_bullet)
            all_sprites_in.add(new_bullet)
            print ("bullet_test")
            print (index)