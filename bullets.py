import pygame
from pygame.locals import (
    RLEACCEL
)
import math

"""
The bullet class renders bullets and helps with collision detection. 
All bullets use this class but the various pygame sprite groups change the purpose of the bullet.
"""

class Bullet (pygame.sprite.Sprite):

    def __init__(self, image_in:str, entity_rect_in:int, speed:int, angle:int , x_position_offset_in:int = 0, y_position_offset_in:int = 0):
        super(Bullet, self).__init__()
        self.surf = pygame.image.load (image_in).convert()
        self.surf.set_colorkey ((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                entity_rect_in.x + x_position_offset_in,
                entity_rect_in.y + y_position_offset_in
            )
        )
        self.speed = speed
        self.angle = angle
        self.x_position = entity_rect_in.x + x_position_offset_in
        self.y_position = entity_rect_in.y + y_position_offset_in
        

    def update (self):
        x_movement = math.cos(math.radians(self.angle))*self.speed
        y_movement = math.sin(math.radians(self.angle))*self.speed
        self.x_position += x_movement
        self.y_position += y_movement
        self.rect.x = int(self.x_position)
        self.rect.y = int(self.y_position)
        if self.rect.right < 0:
            self.kill
        elif self.rect.left > 800:
            self.kill

        