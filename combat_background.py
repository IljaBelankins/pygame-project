import pygame
from pygame.locals import (
    RLEACCEL
)

"""
Code that facilitates the creation of a background image during battle for aesthetic purposes. 
"""

class Combat_Background (pygame.sprite.Sprite):
    def __init__(self, convert_enemy_background:str):
        super(Combat_Background, self).__init__()
        self.surf = pygame.image.load(convert_enemy_background).convert()
        self.surf.set_colorkey ((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect()