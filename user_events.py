import pygame

"""
This file contains all the user events in the game. Necessary to make pygame module run correctly.
"""

ADDENEMY = pygame.USEREVENT + 1
ADDENEMYBULLET = pygame.USEREVENT + 2
STOPENEMYBULLET = pygame.USEREVENT + 3
ADDPLAYERBULLET = pygame.USEREVENT + 4
STOPPLAYERBULLET = pygame.USEREVENT + 5
ADDPLAYERSHIELD = pygame.USEREVENT + 6
STOPPLAYERSHIELD = pygame.USEREVENT + 7