import pygame
from attacks import Attack
from typing import Dict
from pygame.locals import (
    RLEACCEL
)

"""
The enemy class generates the player's opponent. 
The sprite input determines how the enemy looks and adjusts hitbox of the enemy based on image resolution.
The enemy background input works together with the combat_background.py to render the background of the fight.
The attack_dictionary_in is used to generate the enemy's attack pattern.
The bullet_sprite_in determines how this enemy's bullets look and adjusts the hitbox of the bullet based on image resolution.
Screen width and height inputs help with enemy positioning. 
"""

class Enemy (pygame.sprite.Sprite):
    def __init__ (self, enemy_sprite_in:str, enemy_background_in:str, attack_dictionary_in:Dict, bullet_sprite_in:str, screen_width_in:int, screen_height_in:int):
        super(Enemy, self).__init__()
        self.surf = pygame.image.load(enemy_sprite_in).convert()
        self.surf.set_colorkey ((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                screen_width_in - self.surf.get_width(),
                screen_height_in/2
            )
        )
        self.alive = False
        self.speed = 5
        self.health = 5
        self.enemy_background = enemy_background_in
        self.pattern_bullets = attack_dictionary_in["bullets"]
        self.pattern_degrees = attack_dictionary_in["degrees"]
        self.pattern_waves = attack_dictionary_in["waves"]
        self.pattern_sprite = bullet_sprite_in
        self.health = attack_dictionary_in["health"]
        self.screen_width = screen_width_in
        self.screen_height = screen_height_in
        

    def update (self):
        self.rect.move_ip(0, self.speed)
        
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > self.screen_width:
            self.rect.right = self.screen_width
        if self.rect.top <= 0:
            self.rect.top = 0
            self.speed = 5
        if self.rect.bottom >= self.screen_height:
            self.rect.bottom = self.screen_height
            self.speed = -5 

    def get_attack(self):
        return Attack(self.pattern_sprite, self.pattern_bullets, self.pattern_degrees, self.pattern_waves)